# To be sourced not executed

# Only for interactive shells
# test for an existing bus daemon
# Test for dbus-launch executable
if ( $?prompt && $?USER && ( ! $?DBUS_SESSION_BUS_ADDRESS ) ) then

    switch ( "$OSTYPE" )

        case "[Dd]arwin*":

            setenv DBUS_SESSION_BUS_ADDRESS \
                `launchctl getenv DBUS_LAUNCHD_SESSION_BUS_SOCKET`
            # Did launchctl return a value?
            if ( "$DBUS_SESSION_BUS_ADDRESS" == "" ) then
                unsetenv DBUS_SESSION_BUS_ADDRESS
            endif
            breaksw

        default:

            # TODO How in linux etc?
            # Something to do with ~/.dbus/session-bus/biguuid-${DISPLAY} ?
            # Pipe thru `cut -d '=' -f 2-` and assign to env vars?
            breaksw

    endsw

    if ( ( ! $?DBUS_SESSION_BUS_ADDRESS ) && ( -X dbus-launch ) ) then

        ## if not found, launch a new one
        eval `dbus-launch --csh-syntax`
        echo "D-Bus per-session daemon address is: $DBUS_SESSION_BUS_ADDRESS"

        # If we're running under screen, something went wrong somewhere higher
        # in the call chain; screen should only be invoked after a dbus session
        if ( $?WINDOW ) then

            echo 'screen did NOT set DBUS_SESSION_BUS_ADDRESS. Assigning in screen.'
            screen -X setenv DBUS_SESSION_BUS_ADDRESS "$DBUS_SESSION_BUS_ADDRESS"

        endif

    endif

endif
