#!/usr/bin/env bats

load "${HOME}/bin/scriptlib.source.bash"

@test "Capitalize punctuation" {
    result=$(toCapital '[string]')
    [ $result = '[String]' ]
}

@test "Capitalize no punctuation" {
    result=$(toCapital 'string')
    [ $result = 'String' ]
}

@test "Trim" {
    result=$(trim ' string ')
    [ $result = 'string' ]
}
