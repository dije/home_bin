#!/usr/bin/env bash

# To be sourced in bash, not executed.

# Ensure a working EMACS_SERVER_FILE env var if possible

debug(){
    (( ${DEBUG_EC:=0} )) || return
    printf "${HOST_COLORS}Debug:${ANSI_DEFAULT}${0##*/}:%s: %-12s: " "${1}" "${2}" 1>&2
    shift 2
    for x; do
        printf "%s " "${x}" 1>&2
    done
    printf "\n" 1>&2
}

step_spy(){
    (( ${DEBUG_EC:=0} )) || return
    read -p 'Continue...' ignored 1>&2
}

die(){
    echo "${0##*/}: $1" 1>&2
    debug "$LINENO" "Exit status" "${2:-1}"
    exit "${2:-1}"
}

homedir () {
    debug "$LINENO" "\$1" "$1"
    ENTRY="$(getent passwd "$1")" || return
    debug "$LINENO" "ENTRY" "$ENTRY"
    cut -d ':' -f 6 <<< "$ENTRY"
}

HOME_DIR="$(homedir "${EMACS_USER:=phil}")"
debug "$LINENO" "HOME_DIR" "$HOME_DIR"
debug "$LINENO" "EMACS_USER" "$EMACS_USER"
REMOTE_EMACS_SERVER_FILE="${HOME_DIR}/server-file-share/emacs_server_file"
debug "$LINENO" "REMOTE_EMACS_SERVER_FILE" "$REMOTE_EMACS_SERVER_FILE"
LOCAL_EMACS_SERVER_FILE="${HOME_DIR}/.emacs.d/server/server"
debug "$LINENO" "LOCAL_EMACS_SERVER_FILE" "$LOCAL_EMACS_SERVER_FILE"

# Default to remote server file
if [[ -z "$EMACS_SERVER_FILE" ]]; then
    export EMACS_SERVER_FILE="$REMOTE_EMACS_SERVER_FILE"
fi

if (( UID != 0 )); then
    # Validate or toggle server file
    if [[ ! -e "$EMACS_SERVER_FILE" ]]; then
        if [[ "$EMACS_SERVER_FILE" == "$REMOTE_EMACS_SERVER_FILE" ]]; then
            export EMACS_SERVER_FILE="$LOCAL_EMACS_SERVER_FILE"
        elif [[ "$EMACS_SERVER_FILE" == "$LOCAL_EMACS_SERVER_FILE" ]]; then
            export EMACS_SERVER_FILE="$REMOTE_EMACS_SERVER_FILE"
        fi
        # Validate toggle
        if [[ ! -e "$EMACS_SERVER_FILE" ]]; then
            # Bail, hope the unix socket works instead
            unset EMACS_SERVER_FILE
        fi
    fi
else
    # FIXME root can't read /mnt/Media WTF
    # Validate or toggle server file
    if sudo -u "$EMACS_USER" [ ! -e "$EMACS_SERVER_FILE" ]; then
        if [[ "$EMACS_SERVER_FILE" == "$REMOTE_EMACS_SERVER_FILE" ]]; then
            export EMACS_SERVER_FILE="$LOCAL_EMACS_SERVER_FILE"
        elif [[ "$EMACS_SERVER_FILE" == "$LOCAL_EMACS_SERVER_FILE" ]]; then
            export EMACS_SERVER_FILE="$REMOTE_EMACS_SERVER_FILE"
        fi
        # Validate toggle
        if sudo -u "$EMACS_USER" [ ! -e "$EMACS_SERVER_FILE" ]; then
            # Bail, hope the unix socket works instead
            unset EMACS_SERVER_FILE
        fi
    fi
fi
debug "$LINENO" "Ensured EMACS_SERVER_FILE" "$EMACS_SERVER_FILE"
