@test "Simple SSH" {
    run bash trampify 'ssh' 'phil' 'luke' ~/.bashrc
    [ "$status" -eq 0 ]
    [ "$output" = '/ssh__COLON_FOR_TRAMP__phil@luke__COLON_FOR_TRAMP__/home/phil/.bashrc' ]
}

@test "Remote sudo" {
    run bash trampify 'sudo' 'root' 'luke' ~/.bashrc
    [ "$status" -eq 0 ]
    [ "$output" = '/ssh__COLON_FOR_TRAMP__phil@luke__PIPE_FOR_TRAMP__sudo__COLON_FOR_TRAMP__root@luke__COLON_FOR_TRAMP__/home/phil/.bashrc' ]
}

@test "visudo" {
    run bash trampify 'sudo' 'root' 'sara' '--' '/etc/sudoers.d/local.tmp'
    [ "$status" -eq 0 ]
    [ "$output" = '/ssh__COLON_FOR_TRAMP__phil@sara__PIPE_FOR_TRAMP__sudo__COLON_FOR_TRAMP__root@sara__COLON_FOR_TRAMP__/etc/sudoers.d/local.tmp' ]
}
