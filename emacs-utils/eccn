#!/usr/bin/env bash

# GUI mode emacsclient with --create-frame and --no-wait options.
# Can be invoked by root and still open in my emacs session

emacsclientlog="${TMPDIR:-$HOME/tmp}/emacsclient.log"

if (( $# == 0 )) ; then

    "${HOME}/editors/emacsclient" --quiet --create-frame --no-wait &> "$emacsclientlog"

else

    if [[ $1 =~ (.+):([0-9]+)$ ]]; then
        _FILES="+${BASH_REMATCH[2]} ${BASH_REMATCH[1]}"
    else
        _FILES=$@
    fi

    if "${HOME}/bin/emacs-utils/emacs_server_is_on_localhost"; then

        "${HOME}/editors/emacsclient" --quiet --create-frame --no-wait $_FILES &> "$emacsclientlog"

    else

        # nano can't understand trampified file names, so don't risk invoking it
        unset ALTERNATE_EDITOR

        # Prepend each relative file pathname argument with the current working
        # directory, then prepend each argument with the TRAMP method ("ssh"), user
        # ("phil") and host.
        "${HOME}/editors/emacsclient" --quiet --create-frame --no-wait \
            `trampify ssh "$USER" "${SYMLINK_HOST,,}" $_FILES` &> "$emacsclientlog"

    fi

fi
