#!/usr/bin/env bash

# TO BE SOURCED, NOT EXECUTED

# Library of reusable functions for Bash shell scripts

# Note that for functions 'debug' and 'step_spy' to work, there must be already
# defined a function 'debugging' which simply exits 0 or 1. For example:
#
#   debugging () {
#       (( "${DEBUG_MY_APP:=0}" ))
#   }
#
# and for function 'log' to work, there must be already defined an environment
# variable 'LOGFILE' (otherwise, 'log' will write to /dev/null)

diagnose () {
    # Print a diagnostic message
    echo -e "$*" 1>&2
}

diagnosen () {
    # Print a diagnostic message without a newline
    echo -ne "$*" 1>&2
}

readp () {
    # Prompt the user for input
    read -p "${1}: "
}

inputp () {
    # Prompt the user for input
    read -p "${1}: "
    echo "$REPLY"
}

readpd () {
    # Prompt the user for defaulted input
    read -ep "${1}: " -i "$2"
}

inputpd () {
    # Prompt the user for defaulted input
    read -ep "${1}: " -i "$2"
    echo "$REPLY"
}

log () {
    echo "$*" >> "${LOGFILE:=/dev/null}"
}

debug () {
    # $1 is the source line number $LINENO
    # Show the value of the variable named by $2, or label value $3 as $2.
    debugging || return
    local -r DEBUG_TEMPLATE="${HOST_COLORS}Debug${ANSI_DEFAULT}:%s:%3s: %-12s: "
    printf "$DEBUG_TEMPLATE" "${0##*/}" "${1}" "${2}" 1>&2
    if (( "$#" == 2 )); then
        eval "printf '%s ' \$${2}" 1>&2
    else
        shift 2
        for x; do
            printf '%s ' "${x}" 1>&2
        done
    fi
    diagnose
}

step_spy () {
    # Pause script
    debugging || return
    readp 'Continue...' 1>&2
}

die () {
    # Print $1, a diagnostic message, and exit with status $2 (default: 1).
    diagnose "${HOST_COLORS}${0##*/}${ANSI_DEFAULT}: $1"
    exit "${2:-1}"
}

toCapital () {
    # A bit smarter/more flexible than ${FOO^}
    local -r BRACKETS_PATTERN='^([[:punct:]])?([^[:punct:]]+)([[:punct:]])?$'
    for x in $*
    do
        local SUFFIX=''
        if [[ "$x" =~ $BRACKETS_PATTERN ]]; then
            echo -n "${BASH_REMATCH[1]}"
            x="${BASH_REMATCH[2]}"
            SUFFIX="${BASH_REMATCH[3]}"
        fi
        local y="${x,,}"
        echo -n "${y^}"
        # Add a space suffix
        echo -n "${SUFFIX} "
    done
}

trim () {
    # Strip leading and trailing whitespace
    local -r STRIP_SURROUNDING_SPACES_PATTERN='^ *(\S(.*\S)?) *$'
    [[ "$1" =~ $STRIP_SURROUNDING_SPACES_PATTERN ]] && \
        echo "${BASH_REMATCH[1]}" || echo "$1"
}

timeout_prompt () {
    # $1: Field name
    # $2: Default value
    # $3: Timeout in seconds
    read -e -p "${1}: " -i "$2" -t "$3"
    # Timed out? Need to default and output an extra newline
    if (( "$?" > 128 )); then
        echo "$2"
        diagnose
    else
        echo "$REPLY"
    fi
}

found_in_path () {
    # $1: The executable in question
    which "$1" &> /dev/null
}

if ! found_in_path homedir ; then
homedir () {
    # $1: username
    ENTRY="$(getent passwd "$1")" || return
    cut -d ':' -f 6 <<< "$ENTRY"
}
fi

uniquify () {
    if [[ -e "$1" ]]; then
        local -r PREFIX="${1%.*}"
        local -r EXTENSION="${1##$PREFIX}"
        local UNIQUE_NAME
        for SUFFIX in $(seq 0 99); do
            UNIQUE_NAME="${PREFIX}_${SUFFIX}${EXTENSION}"
            [[ -e "$UNIQUE_NAME" ]] || break
        done
        echo "$UNIQUE_NAME"
    else
        echo "$1"
    fi
}
