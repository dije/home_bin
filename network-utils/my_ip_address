#!/usr/bin/env bash

# Reads local host's eth0/en0 ip address
# -------------------------------------------------------------------------
# Copyright (c) 2005 nixCraft project <http://cyberciti.biz/fb/>
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------
# This script is part of nixCraft shell script collection (NSSC)
# Visit http://bash.cyberciti.biz/ for more information.
# -------------------------------------------------------------------------

# Adapted by Phil Hudson to add darwin (Mac OS X) branch (tested using GNU grep
# and sed, not the standard darwin versions); fixed typo in variable
# declaration; replaced double spaces with single; defaulted to localhost.

# Get OS name
OS="$(uname)"
IP=""
case "$OS" in

    FreeBSD|OpenBSD)
        IP="$(ifconfig | grep -E 'inet.[0-9]' | grep -v '127.0.0.1' | awk '{ print $2}')"
        ;;

    SunOS)
        IP="$(ifconfig -a | grep inet | grep -v '127.0.0.1' | awk '{ print $2} ')"
        ;;

    Darwin)
        IP="$(ifconfig -u | grep -v 'inet 127.0.0.1' | sed -n 's/\s*inet \(\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}\) .*/\1/p')"
        ;;

    *)
        IP="$(hostname -I)"
        # Strip vexatious trailing whitespace character
        # Stupid hostname WTF
        IP="$(echo $IP)"
        ;;

esac

echo "${IP:-127.0.0.1}"
