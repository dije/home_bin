# To be sourced in tcsh not executed  -*- mode: sh; eval: (sh-set-shell "tcsh") -*-

set last_SSH_TTY_update=`filetest -M "$SSH_TTY_updater"`
if ( -s "$SSH_TTY_updater" ) then
    setenv SSH_TTY `cat "$SSH_TTY_updater"`
else
    unsetenv SSH_TTY
endif
