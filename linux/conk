#!/usr/bin/env bash

debug () {
    (( ${DEBUG_CONK:=0} )) || return
    printf "${HOST_COLORS}Debug:${ANSI_DEFAULT}%s:%s: %-12s: " "${0##*/}" "${1}" "${2}" 1>&2
    shift 2
    for x; do
        printf "%s " "${x}" 1>&2
    done
    printf "\n" 1>&2
}

step_spy () {
    (( ${DEBUG_CONK:=0} )) || return
    read -p 'Continue...' 1>&2
}

die () {
    echo "${0##*/}: $@" 1>&2
    exit 1
}

debug "$LINENO" "HOME" "$HOME"
export XUL_APP_FILE="${HOME}/binaries/conkeror/application.ini"
debug "$LINENO" "XUL_APP_FILE" "$XUL_APP_FILE"
declare -r APP_ENGINE="${CONKEROR_APP_ENGINE:-xulrunner}"
debug "$LINENO" "APP_ENGINE" "$APP_ENGINE"
# 'xulrunner' or 'firefox'

# export GTK2_RC_FILES="${HOME}/.themes/solarized-dark-gtk/gtk-2.0/gtkrc"

# Handle 'remoting' (request for a running Conkeror instance to open an URL)
RUNNING_INSTANCE_PID="$(/usr/bin/pgrep xulrunner)" && {
    debug "$LINENO" "RUNNING_INSTANCE_PID" "$RUNNING_INSTANCE_PID"
    declare -r REMOTING_LOG="${HOME}/conkeror.remoting.log"
    debug "$LINENO" "REMOTING_LOG" "$REMOTING_LOG"
    declare -r PID_DISPLAY="$("${HOME}/bin/linux/pid_display" "$RUNNING_INSTANCE_PID")"
    debug "$LINENO" "PID_DISPLAY" "$PID_DISPLAY"
    cat >> "$REMOTING_LOG" <<EOF
--------------------------------
Remoting: $(date --iso-8601=ns)
Remoting: Running instance PID: $RUNNING_INSTANCE_PID
Remoting: Running instance DISPLAY: $PID_DISPLAY
Remoting: Parameters: $*
EOF
    /usr/bin/env DISPLAY="$PID_DISPLAY" "$APP_ENGINE" $* &>> "$REMOTING_LOG" &
    exit 0
}

# Launch a new Conkeror instance
source agent.source.bash
export CONKEROR_LOG="${HOME}/conkeror.log"
debug "$LINENO" "CONKEROR_LOG" "$CONKEROR_LOG"

export -f debug

if [[ -s "$CONKEROR_LOG" ]]; then
    "${HOME}/bin/conkeror_cycle_log" &
    # Wait for logfile to be moved to tempdir
    sleep 4
    [[ -e "$CONKEROR_LOG" ]] && die '~/conkeror.log has not been moved yet. Aborting.'
fi

# Backup previous session
[[ -n "$BROWSER_PROFILE" ]] && {
    cd "${BROWSER_PROFILE}/sessions"
    touch ./auto-save
    cp --force --backup ./auto-save ./auto-save
    cd
}

# Position the pointer on the main display
xdotool mousemove 0 0

# Wheels up
"$APP_ENGINE" $* &>> "$CONKEROR_LOG" &

# or:
#firefox -app "$XUL_APP_FILE" $* &>> "$CONKEROR_LOG" &
