#!/usr/bin/env bash

_UPGRADE_LOG="${HOME}/tmp/update_apt.log"
_INSTALLED_ON_ENTRY=`mktemp --tmpdir installed_pre_apt_get_update.XXXXXX`
_INSTALLED_ON_EXIT=`mktemp --tmpdir installed_post_apt_get_update.XXXXXX`
_DELTA=`mktemp --tmpdir apt_get_update_installed_delta.XXXXXX`

export SUDO_ASKPASS="${HOME}/bin/sudo-askpass-with-notify"
export http_proxy=""
export https_proxy=""

clearup () {
    rm "$_INSTALLED_ON_ENTRY" "$_INSTALLED_ON_EXIT" "$_DELTA"
    unset _DELTA _UPGRADE_LOG _INSTALLED_ON_ENTRY _INSTALLED_ON_EXIT
#    exit 0
}

redo_reversions () {
    sudo sh <<EOF
# TODO break out in run-all style
[ -f /usr/share/applications/tuxpaint.desktop ] && \
    sed -i 's@Exec=tuxpaint@Exec=/home/phil/bin/linux/holly-tuxpaint@' \
    /usr/share/applications/tuxpaint.desktop

[ -f /usr/share/applications/vlc.desktop ] && \
    sed -i 's@Exec=/usr/bin/vlc.* %U@Exec=/usr/bin/vlc --extraintf http --fullscreen --video-on-top --disable-screensaver --started-from-file %U@' \
    /usr/share/applications/vlc.desktop

echo
echo '--------------'
echo 'Changed config files:'
echo
find /etc/ -iname '*.dpkg-old' -ctime 0
echo '--------------'
echo
EOF
}

dpkg --get-selections > "$_INSTALLED_ON_ENTRY"

sudo apt-get update
apt-available-updates | "$PAGER"

# Accept new conf files
sudo -A apt-get -o Dpkg::Options::="--force-confnew" dist-upgrade

touch "$_UPGRADE_LOG"
chmod 666 "$_UPGRADE_LOG"
printf "\n\n`date`:\n\n" >> "$_UPGRADE_LOG"
sudo -A apt-get upgrade |& tee --append "$_UPGRADE_LOG"

dpkg --get-selections > "$_INSTALLED_ON_EXIT"
diff "$_INSTALLED_ON_ENTRY" "$_INSTALLED_ON_EXIT" > "$_DELTA"
[ -s "$_DELTA" ] && {
    printf "\nDelta:\n" |& tee --append "$_UPGRADE_LOG"
    tee --append "$_UPGRADE_LOG" < "$_DELTA"
}
# List installed kernels
dpkg-query --show --showformat='${Status}\t${binary:Package;-40}${Version}\n' \
    'linux-image*' | grep --invert-match not-installed | \
    sed 's/install ok installed\t//' | sort --version-sort

clearup
sudo -A apt-get autoclean

redo_reversions

test-all
