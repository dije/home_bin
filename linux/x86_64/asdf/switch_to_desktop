# To be sourced in tcsh not executed -*- mode: sh; eval: (sh-set-shell "tcsh") -*-

# Preconditions:
# * Physically reconnected to the LAN, keyboard and display

# Pre-cache sudo credentials
sudo -v

# Diagnostics
alias msg 'echo "$*" >& /dev/stderr'

msg 'Note: External display and wireless keyboard dongle should already be reconnected'
msg 'and keyboard switched on.'
msg ''
msg 'Press Enter to continue…'
set ignored=$<
msg ''

msg 'Switching to external keyboard…'
cd
rm -f .xbindkeysrc.scm
ln -s .xbindkeysrc.desktop.scm .xbindkeysrc.scm

# Prime the keychain
a

is_ethernet_connected && wifi_off

msg 'Uncommenting LAN hosts entries…'
sudo sed -i '/^#192\.168\.1\./ s/^#//' /etc/hosts
sudo sed -i '/^127.0.1.1/ s/ emacshost$//' /etc/hosts

# Reconfigure ssh
msg 'Switching ssh config to desktop…'
cd
if ( -d .ssh ) then
    cd .ssh
    if ( -l config ) then
        rm config
        ln -s config.lan config
    else
        msg '**** ERROR ****: .ssh/config is missing!'
    endif
    cd
else
    msg '**** ERROR ****: .ssh directory is missing!'
endif

mount_shares

msg 'Inverting trackpad scroll direction…'
invert-trackpad-scroll-direction

msg 'Configuring external display…'
xrandr_desktop

# TODO Switch display-orientation configs

# Resume using squid on squidhost for caching
msg 'Setting proxies…'
source "${HOME}/bin/screen-utils/screen-proxies-set"
sxs "${HOME}/bin/screen-utils/screen-proxies-set"

# TODO Notify clients of proxy config change: conkeror, emacs, identica-mode,
# erc, etc. Identica-mode has a toggle-proxy command.

# Reconfigure conkeror
pgrep xulrunner >& /dev/null
# if ( $? == 0 ) then
#     if ( -X conkeror ) then
#         conkeror -e 'set_proxy_session(null, "192.168.1.3", "3128");'
#     endif
# else
#     msg "NOTE: Remember to call M-x set-proxy-session in Conkeror."
# endif
if ( $? == 0 ) then
    msg 'NOTE: Remember to call M-x set-proxy-session in Conkeror.'
endif

# Ensure shared emacs server file is correct
msg 'Configuring connection to Emacs daemon…'
if ( { emacs_server_should_be_on_localhost } ) then
    if ( ! { emacs_server_file_matches_shared_copy } ) then
        if ( -d "${HOME}/server-file-share" ) then
            if ( { emacs_server_is_on_localhost } ) then
                cp "$EMACS_SERVER_FILE" "${HOME}/server-file-share/emacs_server_file"
            endif
            echo "$SHORT_HOST" > "${HOME}/server-file-share/emacs_server_alias"
        endif
    endif
endif

# Messes up filesets otherwise. Requires on-exit mounts and ssh config.
msg 'Clearing Emacs filesets cache…'
clear-filesets-cache
msg 'Switching Emacs workplace options…'
emacsclient_eval 'ph/on-off-road-off'

parallel "${HOME}/bin/network-utils/wu" ::: sara

raa

cd "${HOME}/.tcshrc.d"
rmf ./alias.context
ln -s alias.desktop_context alias.context
source alias.context
cd

msg 'NOTE: Remember to call M-x set-proxy-session in Conkeror.'

unalias msg

ta
