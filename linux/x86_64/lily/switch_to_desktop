# To be sourced in tcsh not executed -*- mode: sh; eval: (sh-set-shell "tcsh") -*-

# Preconditions:
# * Physically reconnected to the LAN, keyboard and display

xrandr | grep -q 'VGA1 connected'
if ( $? == 0 ) then
    # 24-inch on VGA
    set EXTERNAL_DISPLAY = VGA
else
    # 22-inch on DisplayPort
    set EXTERNAL_DISPLAY=DP1
endif
set EXTERNAL_DISPLAY_POS=0x0
set BUILTIN_DISPLAY_POS=1080x1120

# Pre-cache sudo credentials
sudo -v

# Diagnostics
alias msg 'echo "$*" >& /dev/stderr'

msg 'Note: External display and wireless keyboard dongle should already be reconnected'
msg 'and keyboard switched on.'
msg ''
msg 'Press Enter to continue…'
set ignored=$<
msg ''

# Prime the keychain
a

msg 'Mapping hudson-it.ddns.net to sara…'
sudo sed -i '/^192\.168\.1\.4\s/ s/\(.*\S\)\s*$/\1 hudson-it.ddns.net/' /etc/hosts

# Reconfigure ssh
msg 'Switching ssh config to desktop…'
if ( -d /home/phil/.ssh ) then
    cd /home/phil/.ssh
    if ( -l config ) then
        rm config
        ln -s config.lan config
    else
        msg '**** ERROR ****: .ssh/config is missing!'
    endif
    cd -
else
    msg '**** ERROR ****: .ssh directory is missing!'
endif

# Mount network shares
msg 'Mounting network shares…'
/home/phil/bin/linux/mount_shares

msg 'Inverting trackpad scroll direction…'
invert-trackpad-scroll-direction

msg 'Configuring external display…'
xrandr --output LVDS1 --auto --pos "$BUILTIN_DISPLAY_POS" \
       --output "$EXTERNAL_DISPLAY" --auto --pos "$EXTERNAL_DISPLAY_POS"
# TODO Switch display-orientation configs

# Resume using squid on squidhost for caching
msg 'Setting proxies…'
source /home/phil/bin/screen-utils/screen-proxies-set
sxs /home/phil/bin/screen-utils/screen-proxies-set

# TODO Notify clients of proxy config change: conkeror, emacs, identica-mode,
# erc, etc. Identica-mode has a toggle-proxy command.

# Reconfigure conkeror
pgrep xulrunner >& /dev/null
# if ( $? == 0 ) then
#     if ( -X conkeror ) then
#         conkeror -e 'set_proxy_session(null, "192.168.1.3", "3128");'
#     endif
# else
#     msg "NOTE: Remember to call M-x set-proxy-session in Conkeror."
# endif
if ( $? == 0 ) then
    msg 'NOTE: Remember to call M-x set-proxy-session in Conkeror.'
endif

# Ensure shared emacs server file is correct
msg 'Configuring connection to Emacs daemon…'
if ( { /home/phil/bin/emacs-utils/emacs_server_should_be_on_localhost } ) then
    if ( ! { /home/phil/bin/emacs-utils/emacs_server_file_matches } ) then
        if ( -d /home/phil/server-file-share ) then
            if ( { /home/phil/bin/emacs-utils/emacs_server_is_on_localhost } ) then
                cp "$EMACS_SERVER_FILE" /home/phil/server-file-share/emacs_server_file
            endif
            echo lily > /home/phil/server-file-share/emacs_server_alias
        endif
    endif
endif

# Messes up filesets otherwise. Requires on-exit mounts and ssh config.
msg 'Clearing Emacs filesets cache…'
/home/phil/bin/emacs-utils/clear-filesets-cache

/usr/bin/parallel /home/phil/bin/network-utils/wu ::: sara luke

raa
ta

alias locate 'locate -i -d :/mnt/linkstation/.linkstation.db:/mnt/Media/.Media.db:/mnt/Video/.Video.db !* | p'

msg 'NOTE: Remember to call M-x set-proxy-session in Conkeror.'

unalias msg
rmf ~/.ssh/cp/master-phil_hudson-it.ddns.net_{220,44}3
