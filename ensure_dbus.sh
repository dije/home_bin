#!/usr/bin/env bash

# To be sourced not executed

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# test for an existing bus daemon
# Test for dbus-launch
[ -z "$DBUS_SESSION_BUS_ADDRESS" && -x "`which dbus-launch`" ] || return

## if not found, launch a new one
eval `dbus-launch --sh-syntax`
echo "D-Bus per-session daemon address is: $DBUS_SESSION_BUS_ADDRESS"

# If we're running under screen, something went wrong somewhere higher in
# the call chain; screen should only be invoked after a dbus session.
[ -z "$WINDOW" ] && return

echo 'screen did NOT export DBUS_SESSION_BUS_ADDRESS. Assigning in screen.'
screen -X setenv DBUS_SESSION_BUS_ADDRESS "$DBUS_SESSION_BUS_ADDRESS"
