#!/usr/bin/env bats

load "${HOME}/bin/bash/path_add_declaration"

@test "Existing dir" {
    result=$(path_add "/foo/bar" "/baz/frob:/foo/bar:quux/jeng")
    [ $result = "/baz/frob:/foo/bar:quux/jeng" ]
}

@test "Existing dir, appending" {
    result=$(path_add "/foo/bar" "/baz/frob:/foo/bar:quux/jeng" at_end)
    [ $result = "/baz/frob:/foo/bar:quux/jeng" ]
}

@test "Appending" {
    result=$(path_add "/foo/bar" "/baz/frob:quux/jeng" at_end)
    [ $result = "/baz/frob:quux/jeng:/foo/bar" ]
}

@test "Prepending" {
    result=$(path_add "/foo/bar" "/baz/frob:quux/jeng")
    [ $result = "/foo/bar:/baz/frob:quux/jeng" ]
}
