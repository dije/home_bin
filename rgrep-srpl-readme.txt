
I wrote a bash script that lets you search and replace text strings 
in files of a specified folder and its subfolders recursively. 

Key features:

- recursive operation with immediate results in simulation mode 
  (no waiting time)
- perl/POSIX compliant regex for search strings
- retains ownership and permissions of replaced files
- cross-platform (BSD/Mac/POSIX Linux compatible)
- easy to compare ANSI-colorized diff output (switchable)
- safe: creates and removes temporary files in the system temporary folder
- simulation mode
- free :)


One of the main reasons for me to write this script compared to existing alternatives 
was a possibility to implement simulation mode (fake replacement demonstration mode) 
combined with presenting clear results, in order to quickly distinguish between 
searched and replaced text. 

In simulation mode (switch -s) no actual replacements will be performed, 
but you can see what is being replaced (with ANSI color diff output, if colors 
are supported by your terminal; even work with pipes). 

By utilising 'while read file' loop it is possible to see results coming at once with 
no delay, so long as there are matched strings found -- makes perfect sense for a 
long tree of folders and sub-folders -- no waiting time until script finishes. 

provided utilities:

srpl - search and replace text in multiple files and folders
rgrep - search for text in multiple files and folders



contact information: loco@andrews.lv


