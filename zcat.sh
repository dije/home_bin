#!/usr/bin/env bash

# Adapted from http://www.intuitive.com/wicked/showscript.cgi?037-zcat.sh

# zcat, zmore, zless, and zgrep - this script should be either symbolically
#   linked or hard linked to all four names - it allows users to work
#   with compressed files transparently.

# First step is to try and isolate the filenames in the command line
# we'll do this lazily by stepping through each argument testing to
# see if it's a filename or not. If it is, and it has a compression
# suffix, we'll uncompress the file, rewrite the filename, and proceed.

case "$0" in

  *z*grep*)
    pattern="$1"
    shift
    ;;

esac

for compressed_file
do
  if [ -f "$compressed_file" ] ; then
    case "$compressed_file" in

      *.xz)
        # Delegate to NIH tools
        case "$0" in

          *zcat*   ) xzcat              "$compressed_file";;
          *zmore*  ) xzmore             "$compressed_file";;
          *zless*  ) xzless             "$compressed_file";;
          *zegrep* ) xzegrep "$pattern" "$compressed_file";;
          *zfgrep* ) xzfgrep "$pattern" "$compressed_file";;
          *zgrep*  ) xzgrep  "$pattern" "$compressed_file";;

        esac
        ;;

      *)
        case "$0" in

          *zcat*)
            case "$compressed_file" in

              *.gz|*.Z)
                gunzip --to-stdout "$compressed_file"
                ;;

              *.bz2)
                bunzip2 --stdout "$compressed_file"
                ;;

            esac
            ;;

          *)
            # Use GNU mktemp, not BSD
            case "$OSTYPE" in

                [Dd]arwin*|*BSD)
                    MKTEMP=gmktemp
                    ;;

                *)
                    MKTEMP=mktemp
                    ;;

            esac
            bn="${compressed_file%.*}"
            decompressed_file=`$MKTEMP --tmpdir "${bn//\//_}.XXX"`
            case "$compressed_file" in

              *.gz|*.Z)
                gunzip --to-stdout "$compressed_file" > "$decompressed_file"
                ;;

              *.bz2)
                bunzip2 --stdout "$compressed_file" > "$decompressed_file"
                ;;

            esac
            decompressed_files="${decompressed_files:-""} ${decompressed_file}"
            ;;

        esac
        ;;

    esac
  fi
done

if [ -n "$decompressed_files" ]; then
    case $0 in

        *zmore*  ) more            $decompressed_files;;
        *zless*  ) "$PAGER"        $decompressed_files;;
        *z*grep* ) grep "$pattern" $decompressed_files;;
        *        ) echo "$0: unknown base name. Can't proceed." >&2; exit 1

    esac

    rm $decompressed_files
fi

# and done

exit 0
