#!/bin/tcsh -f

# Sets up your INFOPATH environment variable, used by the 'info' unix shell
# command to provide detailed hypertext documentation on the other unix shell
# commands your system offers.
#
# Uses all of the following:
#   o Your pre-configured INFOPATH (if any). Validates each entry.
#   o Cached results from the last run of buildINFOPATH (if any). Validates
#     each entry.
#   o Heuristic (best-guess) searching for directories containing info files.
#   o Your pre-configured locations to search (if any).
#   o Your pre-configured directories not to search (if any).
#
# What it does:
#   o Dynamically scans for three kinds of probable info directories:
#     o Directories named `info'
#     o Directories containing non-ASCII files named `dir'
#     o Directories containing non-ASCII files named `*.info*'
#   o Adds them to INFOPATH.
#   o Prepends `~/info'
#   o Prepends `.' (telling info to search the current working directory also).
#   o Appends `:' (telling info to append its own internally-defined default
#     INFOPATH).
#   o Persists INFOPATH to file `~/.tcshrc.d/infopath'.
#   o Writes the new value of INFOPATH to standard output.
#
# Usage: buildINFOPATH
#
# Environment variables:
#   INFOPATH: Any valid pre-existing paths are preserved intact.
#
# Files:
#   ~/.tcshrc.d/infopath: For persisting/caching results of this operation.
#                         Read on entry (if it exists) and written on exit.
#   ~/info/.excluded_info_dirs: Vertical-bar-separated list of directories,
#                       sub-directories of which contain false-positive matches
#                       for filename pattern `*.info*' (that is, files whose
#                       names misleadingly make them look like info files). The
#                       vertical bars must be escaped:
#                                 /opt/local/var\|/usr/local/mk
#                       Technically, this list is a single regular expression
#                       containing a set of alternative matches.
#
#
# Required unix shell commands in your PATH:
#   grep
#   find
#     NB gnu find, not the `find' that comes standard on Solaris, Mac OS X, BSD
#   tr
#   echo
#   uniq

# TODO ensure ~/info/ contains file 'dir' by building it if needed

alias msg 'echo "\!:*" >& /dev/stderr'

set _infopath

set _pre_paths

set -r _infopath_file = "${HOME}/.tcshrc.d/infopath"
set -r _excluded_info_dirs_file = "${HOME}/info/.excluded_info_dirs"

# Init which potential info directory matches to exclude:
if ( -r "$_excluded_info_dirs_file" ) then
    set -r _excluded_info_dirs = `cat "$_excluded_info_dirs_file"`
else
    set -r _excluded_info_dirs = "/// Extremely unlikely pattern ///"
endif

msg
msg "Excluded directories regexp:"
msg
msg "$_excluded_info_dirs"
msg

# Parse any environment INFOPATH paths:
if ( $?INFOPATH ) then
    set -f _pre_paths = ( `echo $INFOPATH | tr ':' ' '` )
endif

# Parse any persisted INFOPATH paths:
if ( -r "$_infopath_file" ) then
    set -f _pre_paths = ( $_pre_paths `cat $_infopath_file` )
endif

# Validate the above pre-existing info paths (if any):
foreach _pre_path ( $_pre_paths )
    echo $_pre_path | grep -qv "$_excluded_info_dirs" || continue
    if ( -dr "$_pre_path" ) set -f _infopath = ( $_infopath "$_pre_path" )
end

unset _pre_path*

# Init where to search for info files and directories:
if ( -dr "/usr" ) set _info_search_trees = ( "/usr" )
if ( -dr "/opt" ) set -f _info_search_trees = ( $_info_search_trees "/opt" )

msg "Directory trees to search for info files:"
foreach _info_search_tree ( $_info_search_trees )
    msg "  $_info_search_tree"
end
unset _info_search_tree
msg

# Dynamically add directories containing files called '*.info*':
set -f _infopath = (                           \
    `find -L "${HOME}/.emacs.d/site-lisp/"     \
    -type f -name dir -printf "%h\n"         | \
    grep -v "$_excluded_info_dirs"`            \
    $_infopath                                 \
    `find -H $_info_search_trees \(            \
        -type d \(                             \
            -name '.git'                    -o \
            -name 'RCS'                     -o \
            -name '.bazaar'                 -o \
            -name '.bzr'                    -o \
            -path '/opt/local/var/run/sudo' -o \
            -path '/var/log'                -o \
            -path "${HOME}/bzr_scm" \)         \
        -prune \)                              \
    -o \(                                      \
        -type d                                \
        -name info                             \
        -print \)                              \
    -o \(                                      \
        -type f \(                             \
            -name 'dir'                     -o \
            -name '*.info'                  -o \
            -name '*.info.gz' \)               \
        -printf "%h\n" \)                    | \
    uniq                                     | \
    grep -v "$_excluded_info_dirs"`            \
    )

set -f _infopath = ( "${HOME}/info" $_infopath )

# Exclude empty dirs - 23.2 barfs, who knows why
set _final_infopath = ()
foreach _info_dir ( $_infopath )
    if ( `ls -1 $_info_dir|wc -l` == 0 ) continue
    cd "$_info_dir"
    set _data = 0
    if ( -rf "dir" && `file -b "dir"` == 'data' ) then
        set _data = 1
    else
        set _data = `find . -maxdepth 1 -iname '*.info.gz'|wc -l`
        if ( ! $_data ) then
            foreach _file_to_type ( * )
                if ( -rf "$_file_to_type" ) then
                    # ???? Should this simply be a test for a data file called 'dir'?
                    if ( `file -b "$_file_to_type"` != data ) continue
                    set _data = 1
                    break
                endif
            end
        endif
    endif
    if ( $_data ) then
        set _final_infopath = ( $_final_infopath "$_info_dir" )
    endif
end
unset _info_dir _infopath
cd

set -f _final_infopath = ( "." $_final_infopath ":" )

setenv INFOPATH "`echo $_final_infopath | tr ' ' ':'`:"
unset _final_infopath

echo $INFOPATH | tr ':' '\n' > $_infopath_file

msg
msg "New INFOPATH:"
msg
echo "${INFOPATH}" | tr ':' '\n'


# Local variables:
# mode: sh
# eval: (sh-set-shell "tcsh")
# End:

# End of buildINFOPATH
