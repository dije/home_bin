#!/usr/bin/env bats

load "${HOME}/bin/newhost/newhostlib.sh"

@test "diag writes to stderr" {
    result=$(diag 'foo' 2> /dev/null)
    [ $? -eq 0 ]
    [ -z "$result" ]
    run diag 'foo'
    [ "$status" -eq  0 ]
    [ "$output" ]
}

@test "die exits 1" {
    run die 'foo'
    [ "$status" -eq 1 ]
    [ "$output" ]
}

@test "correct usage" {
    run usage 2 2 'foo'
    [ "$status" -eq 0 ]
    [ -z "$output" ]
}

@test "incorrect usage" {
    run usage 1 2 'foo'
    [ "$status" -eq 1 ]
    [ "$output" ]
}

@test "confirm run_or_skip" {
    echo 'y' | run_or_skip 'foo'
}

@test "refuse run_or_skip" {
    ! echo 'n' | run_or_skip 'foo'
}

@test "ensure_dir" {
    local -r dirname="${HOME}/_stupid_/_old_"
    local -r filename="${dirname}/_name_"
    [ ! -d "$dirname" ]
    ensure_dir "$filename"
    [ -d "$dirname" ]
    [ ! -e "$filename" ]
    rm -R "${HOME}/_stupid_"
}
