#!/usr/bin/env bash

# TO BE SOURCED, NOT EXECUTED

diag () {
    echo $* 1>&2
}

diagn () {
    echo -n $* 1>&2
}

readp () {
    read -p "$1"
}

readpd () {
    read -ep "$1" -i "$2"
}

die () {
    diag $*
    exit 1
}

# $1: Expected argument count
# $2: Actual argument count
# $3: Usage message
usage () {
    [[ $1 == $2 ]] || die "Usage: $3"
}

run_or_skip () {
    read -ep "Run ${1}? (y/n): " -i 'y'
    [[ "$REPLY" =~ [Yy] ]]
}

mkdir_bzr_add () {
    mkdir -p "$1"
    bzr add "$1"
}

ensure_dir () {
    [[ -e "$1" ]] || {
        local -r TARGET_DIR=$(dirname "$1")
        [[ -d "$TARGET_DIR" ]] || {
            diagn "Creating target directory for link: ${TARGET_DIR}..."
            mkdir -p "$TARGET_DIR"
            diag done.
        }
    }
}

setup_host_link () {
    local -r LINK_TO_CREATE="${1}"
    [[ -L "$LINK_TO_CREATE" ]] || {
        local -r REPLACED="${LINK_TO_CREATE}~"
        [[ -e "$REPLACED" ]] && diag "Warning: pre-existing file ${REPLACED}"
        "${HOME}/bin/smart_config_link" "$LINK_TO_CREATE" "${2:+keeping}"
        # Make sure there's something there to link to
        local -r TARGET="$(readlink "$LINK_TO_CREATE")"
        ensure_dir "$TARGET"
        [[ -e "$TARGET" ]] || {
            if [[ -e "$REPLACED" ]]; then
                # No longer needed here (smart_config_link automates this)
                # mv "$REPLACED" "$TARGET"
                :
            elif [[ -z "$3" ]]; then
                touch "$TARGET"
            else
                mkdir -p "$TARGET"
            fi
        }
        bzr add "$TARGET"
    }
}

function setup_host_link_and_edit_target() {
    setup_host_link "$@"
    local -r TARGET=$(readlink "$1")
    readp "Now edit ${TARGET}:"
    nano "$TARGET"
}

#FIXME Does not preserve suffix!
# $1: Link to be created
# $2: OS
setup_os_link () {
    local -r LINK_TO_CREATE="${HOME}/${1}"
    [[ -L "$LINK_TO_CREATE" ]] || {
        ensure_dir "$LINK_TO_CREATE"
        local -r TARGET="${HOME}/.sysconf/${2}${LINK_TO_CREATE}.${2}"
        ensure_dir "$TARGET"
        [[ -e "$LINK_TO_CREATE" ]] && mv "$LINK_TO_CREATE" "$TARGET"
        ln -s "$TARGET" "$LINK_TO_CREATE"
        # Make sure there's something there to link to
        [[ -e "$TARGET" ]] || touch "$TARGET"
        bzr add "$TARGET"
    }
}

found_in_path () {
    which "$1" &> /dev/null
}

etc_edit () {
    local -r ETC_FILE="/etc/${1}"
    readp "Press Enter to edit $ETC_FILE"
    sudo nano "$ETC_FILE"
    local -r SCM_FILE="$(sysconf)${ETC_FILE}"
    ensure_dir "$SCM_FILE"
    cp "$ETC_FILE" "$SCM_FILE"
    bzr add "$SCM_FILE"
}
