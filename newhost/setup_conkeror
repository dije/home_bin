#!/usr/bin/env bash

# $1: os

source ~/bin/newhost/newhostlib.sh

usage 1 "$#" "$0 os"

# Download conkeror source
case "$1" in

    [Dd]arwin*)
        cd "${HOME}/Applications"
        ;;

    *)
        mkdir -p "${HOME}/binaries"
        cd "${HOME}/binaries"
        ;;

esac

export OSTYPE="${OSTYPE:-${1}}"
"${HOME}/bin/conkeror-utils/update_conkeror"

# Setup profile directory and symlink

cd

link=".conkeror.mozdev.org"
replaced="${link}~"
[[ -e "$link" && -e "$replaced" ]] && \
    diag "Warning: pre-existing file/directory ${replaced}"

install_icon () {
    [[ "$1" =~ conkeror_([0-9]+)x ]] && \
        xdg-icon-resource install --novendor --context apps \
                          --size "${BASH_REMATCH[1]}" "$1" conkeror
}

case "$1" in

    [Dd]arwin*)
    # FIXME
        ln -s "${HOME}/Library/...."  "$link"
        ;;

    *)
        "${HOME}/bin/smart_config_link" "$link"
        # Make sure there's something there to link to
        target="$(readlink "$link")"
        [[ -e "$target" && ! -d "$target" ]] && \
            mv -i "$target" "${target}.REPLACED"
        mkdir -p "$target"

        [[ -e "${HOME}/.local/share/applications/conkeror.desktop" ]] || \
            ln -s \
               "${HOME}/.sysconf/linux${HOME}/.local/share/applications/conkeror.desktop" \
               "${HOME}/.local/share/applications/"

        found_in_path xdg-mime && {
            xdg-mime default conkeror.desktop x-scheme-handler/http
            xdg-mime default conkeror.desktop scheme-handler/http
            xdg-mime default conkeror.desktop x-scheme-handler/https
            xdg-mime default conkeror.desktop scheme-handler/https
            # Hey ho
            xdg-mime default chromium.desktop x-scheme-handler/mailto
            xdg-mime default chromium.desktop scheme-handler/mailto
        }

        found_in_path gconftool-2 && {
            gconftool-2 --set /desktop/gnome/url-handlers/http/command \
                        "${HOME}/bin/linux/conk '%s'" --type String
            gconftool-2 --set /desktop/gnome/url-handlers/http/enabled \
                        --type Boolean true
            gconftool-2 --set /desktop/gnome/url-handlers/https/command \
                        "${HOME}/bin/linux/conk '%s'" --type String
            gconftool-2 --set /desktop/gnome/url-handlers/https/enabled \
                        --type Boolean true
        }

        [[ -d "${HOME}/bin/newhost/conkeror_icons" ]] && found_in_path xdg-icon-resource && {
            cd "${HOME}/bin/newhost/conkeror_icons"
            install_icon ./conkeror_128x128x32.png
            install_icon ./conkeror_16x16x32.png
            install_icon ./conkeror_256x256x32.png
            install_icon ./conkeror_32x32x32.png
            install_icon ./conkeror_48x48x32.png
            install_icon ./conkeror_512x512x32.png
            cd -
        }

        # We should already have .local/share/applications/mimeapps.list

        ;;

esac
