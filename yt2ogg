#!/usr/bin/env bash

# Download a YouTube video and extract the music file from it.
# Optional 2nd arg says how many words to take from 'title' to use as 'artist'
declare -r LOGFILE="${HOME}/tmp/yt2ogg.log"

debugging () {
    (( ${DEBUG_YD:=0} ))
}

if debugging ; then
    YTDL_VERBOSITY=' '
else
    YTDL_VERBOSITY=' --quiet '
fi
declare -r YTDL_VERBOSITY

declare -r DEFAULT_YD_FORMATS='43/18/251/140/250/249/medium/best[ext=mp4]/best'
declare -r YTDL_FORMATS=" -f '${YD_FORMATS:=$DEFAULT_YD_FORMATS}' "

source scriptlib.source.bash

exec >& >(tee -a "$LOGFILE")

declare -r OUTPUT_DIR="${HOME}/ripped"
debug "$LINENO" OUTPUT_DIR
mkdir -p "$OUTPUT_DIR" || exit 2
cd "$OUTPUT_DIR" || exit 2

declare -r ADDRESS="$1"
debug "$LINENO" ADDRESS
declare -r YT_DL="youtube-dl"
debug "$LINENO" YT_DL

log '------------------'
log "address: $ADDRESS"

VIDEO_ID="$($YT_DL --get-id "$ADDRESS" 2>&1)"
(( $? != 0 )) && \
    [[ "$VIDEO_ID" =~ unavailable|refused ]] && \
    diagnose "$VIDEO_ID" && exit 254
declare -r VIDEO_ID
debug "$LINENO" VIDEO_ID
log "id: $VIDEO_ID"

# Use yt-dl to query for the video's full title (usually includes artist)
VIDEO_FULL_NAME="$($YT_DL --get-title "$ADDRESS")" || die "Not found: $ADDRESS" 1
declare -r VIDEO_FULL_NAME
debug "$LINENO" VIDEO_FULL_NAME

# ... and file name for output
FFMPEG_INPUT_FILE="${OUTPUT_DIR}/$($YT_DL --get-filename "$ADDRESS")" || \
    die "Not found: $ADDRESS" 2
debug "$LINENO" FFMPEG_INPUT_FILE
declare -r FFMPEG_INPUT_FILE_ALT="${FFMPEG_INPUT_FILE%.*}.mkv"
debug "$LINENO" FFMPEG_INPUT_FILE_ALT
declare -r FFMPEG_INPUT_FILE_ALT2="${FFMPEG_INPUT_FILE%.*}.mp4"
debug "$LINENO" FFMPEG_INPUT_FILE_ALT2
declare -r FFMPEG_INPUT_FILE_ALT3="${FFMPEG_INPUT_FILE%.*}.webm"
debug "$LINENO" FFMPEG_INPUT_FILE_ALT3

diagnose "Artist/title: $VIDEO_FULL_NAME"

declare -r SPLIT_AROUND_DASH_PATTERN='^(.+) -+ (.+)$'
if [[ "$VIDEO_FULL_NAME" =~ $SPLIT_AROUND_DASH_PATTERN ]]; then
    CANDIDATE_VIDEO_ARTIST="${BASH_REMATCH[1]}"
    CANDIDATE_VIDEO_TITLE="${BASH_REMATCH[2]}"
else
    # Heuristic to guess the artist
    CANDIDATE_VIDEO_ARTIST="$(cut -d ' ' -f "1-${2:-2}" <<< "$VIDEO_FULL_NAME")"

    # Heuristic to guess the title
    CANDIDATE_VIDEO_TITLE="$VIDEO_FULL_NAME"
    declare -r STRIP_LEADING_ARTIST_PATTERN="^${CANDIDATE_VIDEO_ARTIST}(.+)$"
    if [[ "$CANDIDATE_VIDEO_TITLE" =~ $STRIP_LEADING_ARTIST_PATTERN ]]; then
        CANDIDATE_VIDEO_TITLE="${BASH_REMATCH[1]}"
        declare -r STRIP_LEADING_SEPARATOR_PATTERN=' - (.+)'
        if [[ "$CANDIDATE_VIDEO_TITLE" =~ $STRIP_LEADING_SEPARATOR_PATTERN ]]; then
            CANDIDATE_VIDEO_TITLE="${BASH_REMATCH[1]}"
        fi
    fi

    declare -r STRIP_TRAILING_SEPARATOR_PATTERN='^(.+) - ?$'
    if [[ "$CANDIDATE_VIDEO_ARTIST" =~ $STRIP_TRAILING_SEPARATOR_PATTERN ]]; then
        CANDIDATE_VIDEO_ARTIST="${BASH_REMATCH[1]}"
    fi
fi
declare -r CANDIDATE_VIDEO_ARTIST
debug "$LINENO" CANDIDATE_VIDEO_ARTIST
declare -r CANDIDATE_VIDEO_TITLE
debug "$LINENO" CANDIDATE_VIDEO_TITLE

declare VIDEO_ARTIST="$(trim "$(toCapital "$CANDIDATE_VIDEO_ARTIST")")"
declare -r VIDEO_ARTIST
debug "$LINENO" VIDEO_ARTIST
declare VIDEO_TITLE="$(trim "$(toCapital "$CANDIDATE_VIDEO_TITLE")")"
declare -r VIDEO_TITLE
debug "$LINENO" VIDEO_TITLE

"${HOME}/bin/gn" 'Tag artist and title' 'yt2ogg'

declare -r ARTIST="$(timeout_prompt 'Artist' "$VIDEO_ARTIST" 25)"
debug "$LINENO" ARTIST

declare -r TITLE="$(timeout_prompt 'Title' "$VIDEO_TITLE" 27)"
debug "$LINENO" TITLE

YTDL_CMD="${YT_DL}${YTDL_FORMATS}${YTDL_VERBOSITY}'${ADDRESS}'"
declare -r YTDL_CMD
debug "$LINENO" YTDL_CMD
log "YTDL_CMD: $YTDL_CMD"
eval "$YTDL_CMD" || die "YTDL failed!" 3

[[ -e "$FFMPEG_INPUT_FILE" ]] || {
    if [[ -e "$FFMPEG_INPUT_FILE_ALT" ]]; then
        FFMPEG_INPUT_FILE="$FFMPEG_INPUT_FILE_ALT"
    elif [[ -e "$FFMPEG_INPUT_FILE_ALT2" ]]; then
        FFMPEG_INPUT_FILE="$FFMPEG_INPUT_FILE_ALT2"
    elif [[ -e "$FFMPEG_INPUT_FILE_ALT3" ]]; then
        FFMPEG_INPUT_FILE="$FFMPEG_INPUT_FILE_ALT3"
    else
        die "No input file for ffmpeg named either\n\t\"${FFMPEG_INPUT_FILE}\"\nor\n\t\"${FFMPEG_INPUT_FILE_ALT}\".\nExiting." 4
    fi
}
declare -r FFMPEG_INPUT_FILE
debug "$LINENO" FFMPEG_INPUT_FILE
log "ffmpeg_input_file: $FFMPEG_INPUT_FILE"

declare -r BASENAME_PATTERN='(.*)\.(webm|flv|mp4|mkv|m4a)'
debug "$LINENO" BASENAME_PATTERN
[[ "$FFMPEG_INPUT_FILE" =~ $BASENAME_PATTERN ]] || \
    die "Unexpected FFMPEG_INPUT_FILE extension: $FFMPEG_INPUT_FILE" 5
declare -r OGGENC_INPUT_FILE="${BASH_REMATCH[1]}.wav"
debug "$LINENO" OGGENC_INPUT_FILE
ffmpeg -loglevel quiet -i "$FFMPEG_INPUT_FILE" "$OGGENC_INPUT_FILE"

log "oggenc_input_file: $OGGENC_INPUT_FILE"

oe "$OGGENC_INPUT_FILE" "$TITLE" "$ARTIST" "$VIDEO_ID"

cd -
