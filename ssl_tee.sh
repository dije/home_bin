#!/bin/bash

# this allows to dump clear-text form of the exchanges between SSL client and SSL server
# client-side verification has to be disabled of course
# anyway, useful for debugging

# to run this you'll need
# - a unix system
# - socat
# - ncat (part of nmap 5.0 suite)

# Example of use:
# $ ssl_tee 10.23.166.132:443 cat > logs.raw
# ... connect your client to localhost:10443
# ^C

# Then you can inspect the trace of the https exchanges in the logs

REMOTE=$1
FILTER=$2
LOCALPORT=10443
PRINTER=""

# Use GNU mktemp, not BSD
case "$OSTYPE" in

    [Dd]arwin*)
        MKTEMP=gmktemp
        ;;

    *)
        MKTEMP=mktemp
        ;;

esac

tempdir=$($MKTEMP --tmpdir --directory) || exit

backpipe="$tempdir/backpipe"
prettypipe="$tempdir/prettypipe"
mkfifo "$backpipe" "$prettypipe"  || exit

function cleanUp() {
    rm -rf -- "$tempdir"
    if [ -n "$PRINTER" ]; then
        wait $PRINTER
    fi
}

trap "cleanUp" EXIT

# apply a filter to the filesystem output
cat < $prettypipe | $FILTER &
PRINTER=$!

# main loop: listen on $LOCALPORT, and redirect to $IP:443
# can this be done with ncat only ?
socat -T0 OPENSSL:$REMOTE,verify=0 STDIO 0<$backpipe | \
    tee -a $prettypipe | \
    ncat --listen --ssl --keep-open localhost $LOCALPORT | \
    tee -a $prettypipe 1>$backpipe

# Just in case the main loop ends for whatever reason.
# In general, this script should be interrupted by ^C
cleanUp
trap - EXIT
exit
