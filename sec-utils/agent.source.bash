#!/usr/bin/env bash

# To be sourced not executed

# $1: Timeout

case "$OSTYPE" in

    [Dd]arwin*)
        _KEYCHAIN=/opt/local/bin/keychain
        ;;

    *)
        _KEYCHAIN=/usr/bin/keychain
        ;;

esac

cd
$_KEYCHAIN --timeout "${1:-20}" --quiet --nogui --agents "ssh" "${HOME}/.ssh/id_rsa"
unset _KEYCHAIN
source refresh_keychain_environment.source.bash
cd ~-
