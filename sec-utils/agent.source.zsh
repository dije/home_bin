#!/usr/bin/env zsh

# To be sourced not executed
case "$OSTYPE" in

    [Dd]arwin*)
        _KEYCHAIN=/opt/local/bin/keychain
        ;;

    *)
        _KEYCHAIN=/usr/bin/keychain
        ;;

esac

pushd -q "$HOME"
$_KEYCHAIN --timeout "${1:-20}" --quiet --nogui --agents "ssh" "${HOME}/.ssh/id_rsa"
unset _KEYCHAIN
source "${HOME}/bin/refresh_keychain_environment.source.bash"
popd -q &> /dev/null
