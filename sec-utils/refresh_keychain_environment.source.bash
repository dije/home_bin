#!/usr/bin/env bash

# To be sourced (not called or exec'ed) from other bash scripts
# Attempts to source keychain environment settings. Not guaranteed to succeed.
# Does not signal any error if it fails.

_HOST="${HOST}"
if [ ! -e "${HOME}/.keychain/${_HOST}-sh" ]; then
    if [ -n "$DOMAIN" ]; then
        # Massage `HOST.local' into `HOST.foo.bar.tld' format
        _DOT_LOCAL_PATTERN='(.+)\.local$'
        if [[ $_HOST =~ $_DOT_LOCAL_PATTERN ]]; then
            _HOST="${BASH_REMATCH[1]}.${DOMAIN}"
        fi
        unset _DOT_LOCAL_PATTERN
        # Try toggling FQDN?
        if [ ! -e "${HOME}/.keychain/${_HOST}-sh" ]; then
            _FQDN_PATTERN='(.+)(\..+)+$'
            if [[ $_HOST =~ $_FQDN_PATTERN ]]; then
                # Strip DN
                _HOST="$BASH_REMATCH[1]"
            else
                # Append DN
                _HOST="${_HOST}.${DOMAIN}"
            fi
            unset _FQDN_PATTERN
        fi
    fi
fi


if [ -e "${HOME}/.keychain/${_HOST}-sh" ]; then
    # Refresh keychain
    . ${HOME}/.keychain/${_HOST}-sh
fi
unset _HOST
